<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::get(
    '/', function () {
    return view('welcome');
});
Auth::routes();

//Debugbar::disable();



Route::get('/logout', 'Auth\LoginController@logout');



Route::group(['middleware' => 'auth'], function(){

    Route::get('/tasks', 'TaskController@index');
    Route::post('/task', 'TaskController@store');
    Route::delete('/task/{task}', 'TaskController@destroy');


    Route::get('/task/edit{task}','TaskController@edit');
    Route::post('/task/edit{task}','TaskController@editSave');

    Route::get('/users', 'UserController@listUsers');
    Route::get('user/','UserController@profile');


    Route::post('add-file/{task}', 'TaskController@addFile');
    Route::post('task/delete/{task}' , 'TaskController@deleteFile');


});

Route::resource('articles','ArticlesController');

Route::post('/articles/{article}','CommentsController@save');


//Chat

Route::get('/chat', 'chatController@index');

Route::post('sendmessage', 'chatController@sendMessage');




//Social

Route::get('/{provider}','SocialAuthController@redirect');


Route::get('/callback/{provider}', 'SocialAuthController@callback');

