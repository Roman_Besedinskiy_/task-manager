<?php

namespace App\Policies;

use App\Models\Article;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class ArticlePolicy
 * @package App\Policies
 */
class ArticlePolicy
{
    use HandlesAuthorization;


    /**
     * @param User $user
     * @param Article $article
     * @return bool
     */
    public function userCan(User $user, Article $article)
    {
        return $user->id === $article->user_id;
    }

}
