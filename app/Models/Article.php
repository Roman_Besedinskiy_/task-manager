<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;


/**
 * Class Article
 * @package App
 */
class Article extends Model

{
    /**
     * @var string
     */
    protected $table = 'articles';

    /**
     * @var array
     */
    protected $dates = ['published_at'];

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'body',
        'published_at'
    ];

    static public function preview()
    {
        return $articles = DB::table('articles')
            ->leftJoin('comments', function ($query) {
                $query->on('comments.article_id', '=', 'articles.id');

            })
            ->select('articles.*', DB::raw('count(comments.id) as commentsCount'))
            ->groupBy('articles.id')->latest()->paginate(5);


    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag')->withTimestamps();
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }

    public function getTagsListAttribute()
    {
        return $this->tags->pluck('id')->all();
    }

    /**
     * @param $date
     */
    public function setPublishedAtAttribute($date)
    {
        $this->attributes['published_at'] = Carbon::createFromFormat('Y-m-d', $date);
    }

    /**
     * @param $date
     * @return Carbon
     */
    public function getPublishedAtAttribute($date)
    {
        return new Carbon($date);
    }
}
