<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SocialAccount
 * @package App\Models
 */
class SocialAccount extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'provider_user_id', 'provider'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
