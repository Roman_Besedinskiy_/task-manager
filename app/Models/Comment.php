<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Comment
 * @package App\Models
 */
class Comment extends Model
{
    /**
     * @var string
     */
    protected $table = "comments";

    /**
     * @var array
     */
    protected $fillable = [
        'author',
        'content',
        'article_id'
    ];

    public function article(){
        $this->belongsTo('App\Models\Article');
    }
}
