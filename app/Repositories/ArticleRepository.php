<?php

namespace App\Repositories;

use DB;


/**
 * Class ArticleRepository
 * @package App\Repositories
 */
class ArticleRepository
{

    public function forArticles()
    {
        return DB::table('users')->
        select('users.id', 'users.name', 'articles.*')
            ->join('articles', 'articles.user_id', '=', 'users.id');
    }

}