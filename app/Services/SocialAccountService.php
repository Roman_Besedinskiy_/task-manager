<?php

namespace App\Services;

use App\Models\SocialAccount;
use App\Models\User;


use Laravel\Socialite\Contracts\User as ProviderUser;

/**
 * Class SocialAccountService
 * @package App\Services
 */
class SocialAccountService
{
    /**
     * @param ProviderUser $providerUser
     * @param $provider
     * @return static
     */
    public function createOrGetUser(ProviderUser $providerUser , $provider)
    {

        $account = SocialAccount::whereProvider($provider)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {

            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $provider
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {

                $user = User::create([

                    'email' => $providerUser->getEmail(),
                    'name' => $providerUser->nickname,
                ]);
            }

            $account->user()->associate($user);
            $account->save();

            return $user;

        }

    }
}


