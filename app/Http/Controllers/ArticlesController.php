<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Tag;
use App\Models\User;
use App\Repositories\ArticleRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Notifications\InvoicePaid;
use Illuminate\Support\Facades\Notification;

/**
 * Class ArticlesController
 * @package App\Http\Controllers
 */
class ArticlesController extends Controller
{
    protected $articles;

    /**
     * ArticlesController constructor.
     */
    public function __construct(ArticleRepository $articles)
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);

        $this->articles = $articles;

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        $articles = $this->articles->forArticles()->latest()
            ->where('published_at', '<=', Carbon::now())
            ->paginate(5);

        if ($request->ajax()) {
            return view('partial.load-article', compact('articles'));
        }


        $user = $request->user();
        $user->notifications()->delete();


        return view('articles.index', compact('articles'));

    }


    /**
     * Create a new article
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $tags = Tag::pluck('name', 'id');

        return view('articles.create', compact('tags'));
    }


    /**
     * Save a new article
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'title'        => 'required|min:3|max:40',
            'body'         => 'required|max:1440',
            'published_at' => 'required|date'
        ]);


        $article = Auth::user()->articles()->create($request->all());

        $article->tags()->attach($request->input('tags_list'));


        $users = User::all();

        Notification::send($users, new InvoicePaid());


        return redirect('/articles');
    }

    /**
     * @param Article $article
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Article $article)
    {

        $author = $this->articles->forArticles()
            ->select('name')
            ->where('articles.id', $article->id)
            ->first();

        $comments = Article::get()->find($article->id)->comments;

        return view('articles.single', compact('article', 'author', 'comments'));
    }

    /**
     * @param Article $article
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Article $article)
    {

        $tags = Tag::pluck('name', 'id');

        $this->authorize('userCan', $article);

        return view('articles.edit', compact('article', 'tags'));
    }

    /**
     * @param Request $request
     * @param Article $article
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Article $article)
    {

        $this->authorize('userCan', $article);

        $this->validate($request, [
            'title'        => 'required|min:3|max:40',
            'body'         => 'required|max:1440',
            'published_at' => 'required|date'
        ]);

        $article->update($request->all());

        $article->tags()->sync($request->input('tags_list'));

        return redirect('/articles');
    }

    /**
     * @param Article $article
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Article $article)
    {
        $this->authorize('userCan', $article);

        $article->delete();

        return redirect('/articles');
    }

}
