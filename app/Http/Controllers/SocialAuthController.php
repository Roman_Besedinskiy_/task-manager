<?php

namespace App\Http\Controllers;

use App\Services\SocialAccountService;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Socialite;


/**
 * Class SocialAuthController
 * @package App\Http\Controllers
 */
class SocialAuthController extends Controller
{

    /**
     * @param $provider
     * @return mixed
     */
    public function redirect($provider)
    {

        return Socialite::driver($provider)->redirect();
    }


    /**
     * @param SocialAccountService $service
     * @param $provider
     * @return \Illuminate\Http\RedirectResponse
     */
    public function callback(SocialAccountService $service, $provider)
    {


        $user = $service->createOrGetUser(Socialite::driver($provider)->user(), $provider);

        auth()->login($user);

        return redirect()->to('/tasks');
    }
}
