<?php

namespace App\Http\Controllers;


use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Request;
use App\Repositories\TaskRepository;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;


class TaskController extends Controller
{
    /**
     * The task repository instance.
     *
     * @var TaskRepository
     */
    protected $tasks;


    /**
     * Create a new controller instance.
     *
     * @param  TaskRepository $tasks
     * @return void
     */
    public function __construct(TaskRepository $tasks)
    {
        $this->middleware('auth');

        $this->tasks = $tasks;
    }


    /**
     * Display a list of all of the user's task.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            return view('partial.load-task', [
                'tasks' => $this->tasks->forUser($request->user()),
            ]);
        }

        return view('tasks.index', ['tasks' => $this->tasks->forUser($request->user()),]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:60',
            'description' => 'max:255',

        ]);

        $request->user()->tasks()->create([
            'name' => $request->name,
            'description' => $request->description,
        ]);

        return redirect('/tasks');
    }

    /**
     * Destroy the given task.
     *
     * @param  Task $task
     * @return Response
     */
    public function destroy(Task $task)
    {
        $this->authorize('canUser', $task);

        $task->delete();

        return redirect('/tasks');

    }


    public function edit(Task $task)
    {

        $this->authorize('canUser', $task);

        return view('tasks/edit')->with('task', $task);

    }

    /**
     * @param Request $request
     * @param Task $task
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function editSave(Request $request, Task $task)
    {
        $this->authorize('canUser', $task);

        $this->validate($request, [
            'name' => 'required|max:60',
            'description' => 'max:255',
        ]);

        $task->update([
            'name' => $request->name,
            'description' => $request->description,
        ]);

        return redirect('/tasks');

    }


    /**
     *
     * @param Request $request
     * @param Task $task
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addFile(Request $request, Task $task)
    {
        $this->authorize('canUser', $task);

        $this->validate($request, [
            'upload_file' => 'required|max:100'
        ]);

        $file = Input::file('upload_file');

        $name = $file->getClientOriginalName();

        $path = $request->upload_file->storeAs('upload_file' . uniqid(), $name);

        $url_file = array();

        $key = '/' . $path;
        $value = $name;
        $url_file[$key] = $value;

        $res = array_map(function ($k, $v) {
            return "$k - $v";
        }, array_keys($url_file), $url_file);

        $preview = implode(';', $res);

        $task->update([
            'file' => $preview
        ]);


        return redirect()->back();
    }

    /**
     * @param Task $task
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteFile(Task $task)
    {

        $this->authorize('canUser', $task);

        $file = explode(" - ", $task->file);

        Storage::delete($file['0']);

        $task->update([
            'file' => ''
        ]);

        return redirect()->back();
    }


}