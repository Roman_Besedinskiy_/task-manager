<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Comment;
use Illuminate\Http\Request;


class CommentsController extends Controller
{


    public function save(Request $request, $id)
    {

        $this->validate($request, [
            'content'=>'required|min:5|max:400'
        ]);
        $all = $request->all();
        $all['article_id'] = $id;
        $all['author'] = Auth::user()->name;

        Comment::create($all);
        return back()->with('message', 'ss');
    }

}
