<?php

namespace App\Http\Controllers;


use App\Models\User;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{


    public function listUsers()
    {

        $users = User::latest()->get();

        return view('users')->with('users', $users);
    }


    public function profile()
    {

        $id = $_GET['id'];

        $users = User::getUserByID($id);

        if (empty($users)) {
            //abort(500);
            return '<h1>The user does not exist</h1>';
        }

        return view('profile', compact('users'));

    }
}
