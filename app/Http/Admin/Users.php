<?php

namespace App\Http\Admin;


use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use App\Models\Role;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializabl;
use SleepingOwl\Admin\Section;

class Users extends Section
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {

            return AdminDisplay::table()
                ->with('roles')
                ->setHtmlAttribute('class', 'table-primary')
                ->setColumns([
                    AdminColumn::link('name')->setLabel('Username'),
                    AdminColumn::email('email')->setLabel('Email')->setWidth('150px'),
                    AdminColumn::lists('roles.label')->setLabel('Roles')->setWidth('200px'),
                ])->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Username')->required(),
            AdminFormElement::password('password', 'Password')->required()->addValidationRule('min:6'),
            AdminFormElement::text('email', 'E-mail')->required()->addValidationRule('email'),
            AdminFormElement::multiselect('roles', 'Roles')->setModelForOptions(new Role())->setDisplay('name'),
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
