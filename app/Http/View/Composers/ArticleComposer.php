<?php

namespace App\Http\View\Composers;

use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class ArticleComposer
{


    /**
     * Bind data to the view.
     *
     * @param  View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        if (Auth::check()) {
            $count = Auth::user()->notifications->count();
            $view->with('count', $count);
        }

    }


}