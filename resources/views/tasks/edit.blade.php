@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Edit Task</h1>
            {!! Form::open(['url' => 'task/edit'. $task->id ,'class' => '']) !!}
            <div class="form-group">
                {!! Form::label( 'name' , 'Task name' ,['class' => 'fcontrol-labe']) !!}
                {!! Form::text( 'name' , $task->name , ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label( 'description' , 'Description:') !!}
                {!! Form::textarea( 'description' , $task->description , ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                <div class="col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-btn fa-plus"></i>Save
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@stop