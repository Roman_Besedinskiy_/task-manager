@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    New Task
                </div>

                <div class="panel-body">

                    @include('errors.errors')

                    <form action="{{ url('task') }}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">Task name</label>

                            <input type="text" name="name" id="task-name" class="form-control"
                                   value="{{ old('task') }}">

                            <label for="task-text" class="col-sm-3 control-label">Description</label>
                            <textarea name="description" id="task-text" class="form-control"
                                      value="{{ old('task') }}"></textarea>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-plus"></i>Add Task
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            @if (count($tasks) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Current Tasks
                    </div>
                    <div class="panel-load">
                        @include('partial.load-task')
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
