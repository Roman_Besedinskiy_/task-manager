@include('errors.errors')
{!! Form::open(  [ 'method' => 'post'] ) !!}

<div class="form-group">

{!! Form::label( 'content' , 'Comment:') !!}
{!! Form::textarea( 'content' , null , ['class' => 'form-control']) !!}

</div>
<div class="form-group">
    {!! Form::submit( 'Send' , ['class' => 'btn btn-primary form-control']) !!}
</div>


{!! Form::close() !!}
@if(Session::has('message'))
    {{Session::get('message')}}
@endif