@extends('layouts.app')

@section('content')

    <div class="container">
        <article class="article">
            <h2>{{ $article->title }}</h2>
            <div class="body">{{ $article->body }}</div>
            <div class="article-footer">
                <h4>Author: {{$author->name}}</h4>
                <div class="date"><h5>Published at : {{ $article->published_at }}</h5></div>
                @unless($article->tags->isEmpty())
                    <div>
                        <h5>Tags:</h5>
                        <ul>
                            @foreach($article->tags as $tag)
                                <li>
                                    {{$tag->name}}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endunless
            </div>
        </article>


        @can('userCan', $article)
            <div class="btn-wrapper">
                {!! Form::open(  [ 'url'=>'articles'.'/' .$article->id ,'method' => 'delete'] ) !!}
                <button type="submit" class="btn btn-danger">
                    <i class="fa fa-btn fa-trash"></i>Delete
                </button>
                {!! Form::close() !!}
                <a href="{{url('articles/'. $article->id . '/edit' )}}"
                   type="submit"
                   class="btn btn-danger edit-btn">Edit article</a>
            </div>
        @endcan

        <div class="comments">

                @foreach($comments as $comment)
                    <div class="media">
                        <div class="media-body">
                            <div class="media-heading">
                                Author : {{$comment->author}}
                            </div>
                            {{$comment->content}}
                        </div>

                    </div>

                @endforeach

        </div>
        @if( Auth::check() )
            @include('articles.comment')
        @endif
    </div>





@stop