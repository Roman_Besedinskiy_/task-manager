@extends('layouts.app')

@section('content')
@include('errors.errors')
    <div class="container add-article">
        <h1>Edit article</h1>

        {!! Form::model( $article, [ 'url'=>'articles'.'/' .$article->id ,'method' => 'put'] ) !!}

        @include('partial.article-form',['submitButton' => 'Update article'])

        {!! Form::close() !!}
    </div>

@stop
