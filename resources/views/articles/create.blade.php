@extends('layouts.app')

@section('content')
@include('errors.errors')
    <div class="container add-article">
        <h1>Write a New article</h1>
        {!! Form::model($article = new \App\Models\Article,['url' => '/articles']) !!}

        @include('partial.article-form',['submitButton' => 'Create article'])

        {!! Form::close() !!}
    </div>

@stop