@extends('layouts.app')

@section('content')

    <div class="container">
        @if( Auth::check() )
        <div class="new-article">
            <h1><a href="{{ url('articles/create') }}">Create new article</a></h1>
        </div>
        @endif
        <div id="load">
            @include('partial.load-article')
        </div>
    </div>
@stop