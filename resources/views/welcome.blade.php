<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel Task</title>

      <!-- Styles -->

    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" />
</head>

<body>
        <div class="flex-center position-ref full-height">
              @if (Auth::guest())
                <div class="top-right links">
                    <a href="{{ url('/login') }}">Login</a>
                    <a href="{{ url('/register') }}">Register</a>
                </div>
            @else
                <div class="top-right links">
                <a href="{{ url('/tasks') }}">Tasks {{ Auth::user()->name }}</a>
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel Task
                </div>
                <div class="col-md-12 text-center">
                    <p>Login with</p>
                    <p><a href="{{url('/github') }}">Github</a></p>
                </div>

            </div>
        </div>
    </body>
</html>
