@extends('layouts.app')

@section('content')
    <div class="container">
        <table class="table table-striped ">
            <thead>
            <tr>
                <th>User-info</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="table-text">
                    <div class="task-title">name</div>
                </td>

                <td style="text-align: right">
                    {{ $users['0']->name}}
                </td>
            </tr>
            <tr>
                <td class="table-text">
                    <div class="task-title">Email</div>
                </td>

                <td style="text-align: right">

                    {{ $users['0']->email}}
                </td>
            </tr>
            <tr>
                <td class="table-text">
                    <div class="task-title">Password hash</div>
                </td>
                {{--{{ //dd($users)}}--}}
                <td style="text-align: right">
                    {{ $users['0']->password}}
                </td>
            </tr>
            <tr>
                <td class="table-text">
                    <div class="task-title">Created at</div>
                </td>

                <td style="text-align: right">
                    {{ $users['0']->created_at}}
                </td>
            </tr>

            </tbody>
        </table>
    </div>
@endsection