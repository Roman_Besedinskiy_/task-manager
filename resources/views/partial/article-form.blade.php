<div class="form-group">
    {!! Form::label( 'title' , 'Title:') !!}
    {!! Form::text( 'title', null , ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label( 'body' , 'Body:') !!}
    {!! Form::textarea( 'body' , null , ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label( 'published_at' , 'Published on:') !!}
    {!! Form::input( 'date' , 'published_at' , $article->published_at->format('Y-m-d'), ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label( 'tags_list' , 'Tags:') !!}
    {!! Form::select( 'tags_list[]' , $tags , null , ['class' => 'form-control select-tag' ,'multiple']) !!}
</div>

<div class="form-group">
    {!! Form::submit( $submitButton , ['class' => 'btn btn-primary form-control']) !!}
</div>