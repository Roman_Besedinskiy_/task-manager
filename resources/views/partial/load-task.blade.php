<div class="panel-body" id="load">

    @foreach ($tasks as $task)
        <div class="task-wrapper">
            <h2>{{ $task->name }}</h2>

            @if( empty($task->description) == true)
                <p>No Description</p>
            @else
                <h4>Description</h4>
                <p>{{ $task->description }}</p>
            @endif

            @if( empty($task->file) )
                <div class="file-wrapper">
                    <form class="add_file_form" enctype="multipart/form-data"
                          action="{{url('add-file/' . $task->id)}}" method="POST">
                        <input name="upload_file" type="file">
                        {{ csrf_field() }}
                        <input class="btn-info" type="submit" value="Send File"/>
                    </form>
                </div>
            @else
                <div class="file-wrapper">
                    @php
                        $url = explode(" - ", $task->file);
                    @endphp
                    <div class="file-title">Download file</div>
                    <a href="{{ 'http://localhost/task-manager/storage/app/public'.$url[0] }}"><i
                                class="fa fa-file-archive-o" aria-hidden="true"></i>
                        {{ $url['1'] }} </a>
                </div>
            @endif

            <div class="btn-wrapper">
                <form action="{{url('task/' . $task->id)}}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}

                    <button type="submit"
                            id="delete-task-{{ $task->id }}" class="btn btn-danger">
                        <i class="fa fa-btn fa-trash"></i>Delete
                    </button>
                </form>
                <a href="{{url('task/edit' . $task->id)}}"
                   type="submit"
                   id="edit-task-{{ $task->id }}" class="btn btn-danger edit-btn">
                    <i class="fa fa-btn fa-book" aria-hidden="true"></i>Edit</a>

            </div>
        </div>
    @endforeach

    {{$tasks->render()}}
</div>