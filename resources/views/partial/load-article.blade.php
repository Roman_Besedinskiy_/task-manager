
@foreach($articles as $article)
    <article class="article">
        <h2><a href="{{url('articles/'. $article->id)}}">{{ $article->title }}</a></h2>
        <div class="body">{{ $article->body }}</div>
        <div class="article-footer">
            <h4>Author: {{ $article->name }}</h4>
            <div class="date"><h5>Published at : {{ $article->published_at }}</h5></div>
        </div>

    </article>
@endforeach
{{$articles->render()}}