@extends('layouts.app')
@section('content')
<div>
    <h1 class="user-title">List of Registered Users</h1>
    @foreach ($users as $user)
        <h4 class="user-item" ><a href="{{ url('/user/?id=' . $user->id ) }}">{{ $user->name }}</a></h4>
    @endforeach

</div>
@endsection
