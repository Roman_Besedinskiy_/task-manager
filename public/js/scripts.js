/**
 * Created by Roman on 03.11.2016.
 */

$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        $('#load a').css('color', '#dfecf6');
        $('#load').append('<img style="position: absolute; left: 50%; top: 50%; z-index: 100000;" src="img/ajax-loader.gif" />');

        var url = $(this).attr('href');
        getArticles(url);
        window.history.pushState("", "", url);
    });

    function getArticles(url) {
        $.ajax({
            url : url
        }).done(function (data) {
            $('#load').html(data);
        }).fail(function () {
            alert('Tasks could not be loaded.');
        });
    }
});

$(".select-tag").select2({
    tags: false
});


